def open_list(filepath):
    itemlist = []
    file_contents = None
    with open(filepath, 'r') as fileobject:
        file_contents = fileobject.read().splitlines()
    file_contents = [x for x in file_contents if not x.startswith('#')] # remove comments
    file_contents = [x for x in file_contents if not x==''] # remove any blank (usually trailing) lines

    if file_contents[0]=='group_list':
        itemlist = [int(a,16) for a in file_contents[1:]]       # Convert to in from hexadecimal strings
    elif file_contents[0]=='tag_list':
        for row in file_contents[1:]:
            tag = row.split('v')
            itemlist.append((int(tag[0],16),int(tag[1],16)) # Convert to a tuple of ints from hex strings
    elif file_contents[0]=='element_name_list':
        itemlist = file_contents[1:].copy()
    elif file_contents[0]=='id_mapping_list':
        itemlist = [a.split(',') for a in file_contents[1:]]
    return itemlist

def get_tag(ds,tag_name):
    if tag_name in dir(ds):
        return getattr(ds,tag_name)

def get_tag_by_key(ds,tag_key):
    try:
        value = ds[tag_key[0],tag_key[1]].value
        return value
    except:
        # Can't remember what specific exception this raises - KeyError??
        raise

def get_hash(value):
    return hashed_value

def remove_tag(ds,tag_name):
    if tag_name in dir(ds):
        delattr(ds,tag_name)

def replace_tag(ds,tag_name,new_value):
    setattr(ds,tag_name,new_value)

def hash_tag(ds,tag_name):
    if tag_name in dir(ds):
        setattr(ds,tag_name,get_hash(getattr(ds,tag_name)))

def mask_birth_date(ds):
    # CHECK THIS TO SEE IF YOU CAN MODIFY DATES ELEMENT-WISE
    new_PatientBirthDate = ds.PatientBirthDate
    new_PatientBirthDate.day = 1
    new_PatientBirthDate.month = 1
    ds.PatientBirthDate = new_PatientBirthDate
    return ds

def deid_dataset(ds,new_PatientID,trial,safe_private=True,retain_overlays=False,whitelist=[],blacklist=[]):
    new_IssuerOfPatientID = 'RBF_PYTHON'

    ds.PatientID = str(trial)+'_'+str(new_id)
    ds.AccessionNumber = get_hash(ds.AccessionNumber)

    # Hash tags that need hashing
    for element_name in hash_list:
        hash_tag(ds,element_name)

    # Remove tags that need removing
    for element_name in remove_list:
        remove_tag(ds,element_name)

    # Replace tags that need replacing
    # Group 0010
    replace_tag(ds,'PatientName',trial+'^'+new_PatientID)
    replace_tag(ds,'PatientID',new_PatientID)
    replace_tag(ds,'IssuerOfPatientID',new_IssuerOfPatientID)
    replace_tag(ds,'BranchOfService','')
    # Group 0012
    replace_tag(ds,'ClinicalTrialProtocolID',trial)
    replace_tag(ds,'PatientIdentityRemoved','YES')

    # Mask DOB
    ds = mask_birth_date(ds)

    # Deal with overlays
    if not retain_overlays:
        remove_tag(ds,'OverlayDate')
        remove_tag(ds,'CurveDate')
        remove_tag(ds,'OverlayTime')
        remove_tag(ds,'CurveTime')
        remove_tag(ds,'OverlayRows')
        remove_tag(ds,'OverlayColumns')
        remove_tag(ds,'OverlayDescription')
        remove_tag(ds,'OverlayType')
        remove_tag(ds,'OverlaySubtype')
        remove_tag(ds,'OverlayOrigin')
        remove_tag(ds,'OverlayBitsAllocated')
        remove_tag(ds,'OverlayBitPosition')
        remove_tag(ds,'OverlayLabel')
        remove_tag(ds,'OverlayData')

    # Reference UIDs??? (0008,1111) (0008,114A) (0008,1115) (0008,1200) (0008,1140) (0008,1250) (0008,2112) (0008,3010) (0008,9121) (0008,113A) (0008,9092) (0008,9154) (0008,9237) sequences

    # Not adding code to remove patient characteristics
    # Sex, Age, Size, Weight, Allergies, Ethnic Group, Occupation, Pregnancy, Neutered, Smoking Status

    # Group 5200
    # Functional group sequences.... tricky?


    #### PRIVATE TAGS ####

    # Open these and convert to integer/tuple lists in base 10
    whitelist = open_list(private_group_whitelist.txt)
    blacklist = open_list(private_tag_blacklist.txt)

    for element in ds:
        if not element.tag[0]%2 == 0:
            if not element.tag[0] in whitelist or (element.tag.group,element.tag.element) in blacklist:
                del(ds[element.tag])

    # Need to iterate through functional group sequences also for Enhanced MR Image Storage
    if 'SharedFunctionalGroupsSequence' in dir(ds):
        for i in range(len(ds.SharedFunctionalGroupsSequence)):
            ds.SharedFunctionalGroupsSequence[i] = deid_dataset(ds.SharedFunctionalGroupsSequence[i],new_PatientID,trial,safe_private=safe_private,retain_overlays=retain_oberlays,whitelist=whitelist,blacklist=blacklist)

    if 'PerFrameFunctionalGroupsSequence' in dir(ds):
        for i in range(len(ds.PerFrameFunctionalGroupsSequence)):
            ds.PerFrameFunctionalGroupsSequence[i] = deid_dataset(ds.PerFrameFunctionalGroupsSequence[i],new_PatientID,trial,safe_private=safe_private,retain_overlays=retain_oberlays,whitelist=whitelist,blacklist=blacklist)

    return ds
