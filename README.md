# DICOM ANON

## Outline
This is a simple DICOM anonymiser (or technically a pseudonymiser) which takes
an input list of patient IDs and pseudo IDs to map to, and scans a directory
instance by instance in order to remove all identifiable information.
