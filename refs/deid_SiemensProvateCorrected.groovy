// UHB Script v 0.24 - customized for trial 5364 by DW 14/02/2018
//
// Supported SOP Classes:
//              - 1.2.840.10008.5.1.4.1.1.1 (Computed Radiography Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.1.2 (Digital Mammography X-Ray Image Storage - For Presentation)
//              - 1.2.840.10008.5.1.4.1.1.1.2.1 (Digital Mammography X-Ray Image Storage - For Processing)
//              - 1.2.840.10008.5.1.4.1.1.2 (CT Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.4 (MR Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.4.1 (Enhanced MR Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.4.2 (MR Spectroscopy Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.7 (SC Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.12.1 (X-Ray Angiographic Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.12.1 (X-Ray Radiofluoroscopic Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.20 (Nuclear Medicine Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.128 (PET Image Storage)
//
// Basic Application Level Confidentiality Profile options:
//              - Retain Longitudinal Temporal Information (with Modified Dates disabled)
//              - Retain Patient Characteristics (some attributes cleaned)
//              - Retain UIDs
//              - Retain Safe Private
//
// Options _not_ currently supported:
//              - Clean Pixel Data
//              - Clean Graphics
//              - Clean Structured Content
//
// De-identification of non-human patients is not supported
//
//
// DW 14/02/18: Script modified to put patients without CTP mapping in the default bin
//
import com.karos.connectxds.mc.UIDHasher
import java.security.MessageDigest

// ** COMMON FUNCTIONS **

// Date offset function
def dateOffset = { String oldDate, int dateOffset, String defaultDate = null ->

    if (oldDate != null) {
        newDate = new Date().parse('yyyyMMdd', oldDate)
        newDate.setDate(newDate.getDate() + dateOffset)
        return newDate.format('yyyyMMdd')
    }
    if (defaultDate != null) {
        return defaultDate
    }
    newDate = new Date()
    newDate.setDate(newDate.getDate() + dateOffset)
    return newDate.format('yyyyMMdd')
}

// ** END OF COMMON FUNCTIONS **

// ** SITE VARIABLES **

log.info("deid.groovy v24: Now de-identifying Study:SOP Instance {}:{}", get(StudyInstanceUID), get(SOPInstanceUID))
def configFilePath = '/home/rialto/rialto2/etc/services/dicom-router/deid-config/'
def mappingFilePath = '/opt/CTP/scripts/lookup-table.properties'

// UID Hash function
// see http://oid-info.com/get/2.16.840.1.113883.3.2644 for University Hospitals Birmingham NHS Foundation Trust  OID registration
def localOID = '2.16.840.1.113883.3.2644.1.6.1'
UIDHasher hasher = new UIDHasher(localOID)

// Other pre-defined values
def newUniversalEnityID = localOID
def newIssuerOfPatientID = 'ITM'
def replacementYear = new Date().format("yyyy").toInteger() - 90 // current year - 90

// ** END OF SITE VARIABLES **

// ** UNSUPPORTED DATA IDENTIFICATION **

// Decide whether to flag unsupported data or to drop it entirely
def deleteUnsupported = false
def flaggedInstance = false // current SOP Instance starts off as not flagged
def flagUIDSuffix = '' // Flagged data will have an addition Study Instance UID suffix to separate it
def flagAccessionSuffix = '' // Flagged data will have an addition Accession Number suffix to separate it

// Exclude unsupported SOP Classes
def sopClassWhitelist = []
new File(configFilePath + 'sopClassWhitelist.txt').eachLine { sopClass ->
       sopClassWhitelist << sopClass
}

def instanceSOPClass = get(SOPClassUID)
if (!sopClassWhitelist.contains(instanceSOPClass)){
    log.warn("deid.groovy: Instance flagged. SOP Class UID {} unsupported.", instanceSOPClass)
    flaggedInstance = true
}

// Exclude data with burned in PHI flag
def burnedInAnnotation = get(BurnedInAnnotation) 
// KN/MC 22-12/2017 modified line below so that get(BurnedAnnotation) is returned not the boolean status
//def burnedInAnnotation = get(BurnedInAnnotation) == 'YES'
def imageType = getList(ImageType)
if (burnedInAnnotation == 'YES') {
    log.warn("deid.groovy: Instance flagged. Burned In Annotation tag set to 'YES'.")
    flaggedInstance = true
} else if (burnedInAnnotation != 'NO') { 
// If Burned in Annotation has been set to 'NO' it is trusted to be true
// Otherwise check other attributes to see if instance should be flagged

    // Exclude data with Study Description, Series Description or Protocol Name in blacklist
    new File(configFilePath + 'seriesBlackList.txt').eachLine { flaggedSeries ->
        if ([(get(StudyDescription) ?: '').toUpperCase(), (get(SeriesDescription) ?: '').toUpperCase(), (get(ProtocolName) ?: '').toUpperCase()].contains(flaggedSeries.toUpperCase())) {
            log.warn("deid.groovy: Instance flagged. Study Description, Series Description or Protocol Name contains {}.", flaggedSeries)
            flaggedInstance = true
        }
    }

    // Exclude data with Manufacturer Name in blacklist
    new File(configFilePath + 'manufacturerBlackList.txt').eachLine { phiManufacturer ->
        if (phiManufacturer.toUpperCase() == (get(Manufacturer) ?: '').toUpperCase()) {
            log.warn("deid.groovy: Instance flagged. Manufacturer contains {}.", phiManufacturer)
            flaggedInstance = true
        }
    }

    // Exclude data with ConversionType in blacklist
    def phiConversionType = ['SD', 'SI', 'DF', 'DV']
    if (phiConversionType.contains(get(ConversionType))) {
        log.warn("deid.groovy: Instance flagged. Conversion Type is {}.", get(ConversionType))
        flaggedInstance = true
    }

    // Exclude data with Image Type in blacklist
    log.info("deid.groovy: Image Type = {}", imageType)
    def phiImageType = ['DOC', 'SCAN', 'REFORMATTED', 'DI SCAN DOC']

    imageType.each { imageTypeValue ->
        if (phiImageType.contains(imageTypeValue)) {
            log.warn("deid.groovy: Instance flagged. Image Type is {}.", imageType)
            flaggedInstance = true
        }
    }

}

if (deleteUnsupported && flaggedInstance) { // SOP instance is flagged and delete unsupported is true
    return false
} // otherwise keep instance even if it is flagged

// ** END OF UNSUPPORTED DATA IDENTIFICATION **

// ** CLINICAL TRIAL ASSIGNMENT **

// Clinical trial values
def patientCharacteristics = false 
def safePrivate = true 
def retainOverlays = false
def modifiedDates = false 
// Retain overlays for MR spectroscopy data
if (imageType.contains('SPECTROSCOPY') || imageType.contains('SPEC')) {
    log.debug("deid.groovy: Instance is MR Spectroscopy. Overlays will be preserved.")
    retainOverlays = true
}

// load station name whitelist
def stationNames = []
new File(configFilePath + 'stationNameWhitelist.txt').eachLine { foundStationName ->
   stationNames << foundStationName
}

def clinicalTrialID = null
def branchOfService = get(BranchOfService)
def trialStudyDescription = get(StudyDescription)

// Case 1: Branch of Service = RRK15xxxx where xxxx is a number
if ((branchOfService != null) && (branchOfService.startsWith('RRK15')) && (branchOfService.substring(branchOfService.length()-4).isNumber())) {
    clinicalTrialID = branchOfService.substring(branchOfService.length()-4)
}

// Case 2: Study Description = RRKxxxx* where xxxx is a number
if (!clinicalTrialID && trialStudyDescription && (trialStudyDescription.startsWith('RRK')) && (trialStudyDescription.substring(3,7).isNumber())) {
    clinicalTrialID = trialStudyDescription.substring(3,7)
}

// Case 3: AE Title in Station Name List
if (trialStudyDescription && !clinicalTrialID && (stationNames.contains(getCallingAETitle()))) {
    clinicalTrialID = trialStudyDescription.replaceAll(" ", "_")
}

if (flaggedInstance) { // instance is flagged 
    clinicalTrialID = 'DEBUG'
    set(BurnedInAnnotation, 'YES') // Allows this to be edited
    flagUIDSuffix = '.1'
    flagAccessionSuffix = 'X'

} else if (!clinicalTrialID) { // Study UID is different for unknown clincal trial
    flagUIDSuffix = '.2'
    flagAccessionSuffix = 'Y'
}

// At this stage, clinicalTrial ID is one of:
// - null
// - a four digit numerical code
// - the Study Description
// - DEBUG
log.debug("deid.groovy: SOP Instance {}:{} has Clinical Trial ID {}.", get(StudyInstanceUID), get(SOPInstanceUID), clinicalTrialID)

// ** END OF CLINICAL TRIAL ASSIGNMENT **

// ** IDENTIFIER MAPPING **

// input fields mapped
def oldPatientID = get(PatientID)
def oldAccessionNumber = get(AccessionNumber)
if (oldAccessionNumber == null) {
    log.warn("deid.groovy: Missing accession number. Cannot be de-identified.")
    // return false
    // TEST ONLY
    oldAccessionNumber = oldPatientID.reverse()
    // TEST ONLY
}

def newPatientID 
def newAccessionNumber 
def studyOffset

// Lookup Patient ID
def mappingTable = [:]
new File(mappingFilePath).eachLine { fileEntry ->
    if (fileEntry.startsWith('patientID')) { // ignore comments
        fileLineContents = fileEntry.tokenize('/=')
        if (fileLineContents.size() == 4) { // entries with trial ID, patient ID and subject ID
            if (fileLineContents[1].startsWith('RRK')) {
                    fileLineContents[1] = fileLineContents[1].substring(fileLineContents[1].length()-4)
            } 
            if (mappingTable[(fileLineContents[1])] == null) { // initialize trial table first time trial is encountered
                mappingTable[(fileLineContents[1])] = [:]
            }
            mappingTable[(fileLineContents[1])][(fileLineContents[2])] = fileLineContents[3] 
        }
    }
}
log.debug("routing.groovy: Patient ID Mapping Table: \n{}", mappingTable)

// START added DW 14/02/2018
// If no CTP entry, treat as no though no clinical trial ID set

if (mappingTable[clinicalTrialID][oldPatientID] == null) {
        log.warn("deid.groovy: clinicalTrialID {} ignored because patient mapping is absent in CTP.",clinicalTrialID)
        clinicalTrialID = null
}

// END added DW 14/02/2018

// get new PatientID
if ((mappingTable[clinicalTrialID] == null) || (mappingTable[clinicalTrialID][oldPatientID] == null)) { // no mapping table entry
       newPatientID = MessageDigest.getInstance("MD5").digest(oldPatientID.bytes)
       StringBuilder sb = new StringBuilder(); // Messy function required by old Groovy version    
       for (byte b : newPatientID) {         
           sb.append(String.format("%02X", b))
       }     
       newPatientID = sb.toString()
} else {
    newPatientID = mappingTable[clinicalTrialID][oldPatientID]
}

newAccessionNumber = MessageDigest.getInstance("MD5").digest(oldAccessionNumber.bytes)
StringBuilder sb2 = new StringBuilder();  // Messy function required by old Groovy version   
for (byte b : newAccessionNumber) {         
    sb2.append(String.format("%02X", b))
}     
newAccessionNumber = sb2.toString().take(16) + flagAccessionSuffix

// get studyOffset
if (modifiedDates) { // generate study offset
    studyOffset = new BigInteger(1,MessageDigest.getInstance("MD5").digest(newPatientID.getBytes())).mod(365) - 182
} else { // study offset is zero
    studyOffset = 0
}

// ** END OF IDENTIFIER MAPPING **

// ** CONFIDENTIALITY PROFILES **

// Clinical trial mapping to de-identification options
// Always included:
//      - 113100: Basic Application Confidentiality Profile
//      - 113110: Retain UIDs
def confidentialityProfileOptions = ['113100', '113110']

// Options:
//      - 113106: Retain Longitudinal Temporal Information with Full Dates
//      - 113107: Retain Longitudinal Temporal Information with Modified Dates
//      - 113108: Retain Patient Characteristics (some attributes cleaned)
//      - 113111: Retain Safe Private
if (!modifiedDates) {
    confidentialityProfileOptions << '113106'
} else {
    confidentialityProfileOptions << '113107'
}

if (patientCharacteristics) {
    confidentialityProfileOptions << '113108'
}

if (safePrivate) {
    confidentialityProfileOptions << '113111'
}

// ** END OF CONFIDENTIALITY PROFILES **

// ************************
// ** ATTRIBUTE MORPHING **
// ************************

// ** GROUP 0008 **
// add studyOffset to StudyDate (0008,0020)
def newStudyDate = dateOffset(get(StudyDate), studyOffset)
set(StudyDate,newStudyDate)
log.debug("deid.groovy: New Study Date: {}", newStudyDate)

// add studyOffset to InstanceCreationDate (0008,0012)
set(InstanceCreationDate, dateOffset(get(InstanceCreationDate), studyOffset, newStudyDate))
log.debug("deid.groovy: New Instance Creation Date: {}", get(InstanceCreationDate))

def oldInstanceCreatorUID = get(InstanceCreatorUID) // (0008,0014)
if (oldInstanceCreatorUID != null) {
        set(InstanceCreatorUID, hasher.hashUID(oldInstanceCreatorUID))  
} else {
        remove(InstanceCreatorUID)
}

def oldSOPInstanceUID = get(SOPInstanceUID)
if (oldSOPInstanceUID != null) {
        set(SOPInstanceUID, hasher.hashUID(oldSOPInstanceUID)) // (0008,0018)
}

// add studyOffset to SeriesDate (0008,0021)
set(SeriesDate, dateOffset(get(SeriesDate), studyOffset, newStudyDate))
log.debug("deid.groovy: New Series Date: {}", get(SeriesDate))

// add studyOffset to AcquisitionDate (0008,0022)
set(AcquisitionDate, dateOffset(get(AcquisitionDate), studyOffset, newStudyDate))
log.debug("deid.groovy: New Acquisition Date: {}", get(AcquisitionDate))

// add studyOffset to ContentDate (0008,0023)
set(ContentDate, dateOffset(get(ContentDate), studyOffset, newStudyDate))
log.debug("deid.groovy: New Content Date: {}", get(ContentDate))

remove(OverlayDate) // (0008,0024)
remove(CurveDate) // (0008,0025)

// add studyOffset to AcquisitionDateTime (0008,002A)
def newAcquisitionDateTime = new Date()
def oldAcquisitionDateTime = get(AcquisitionDateTime)
log.debug("deid.groovy: Old Acquisition Datetime: {}", oldAcquisitionDateTime)
def dateTimeFormat = 'yyyyMMddHHmmss.SSS'
if (oldAcquisitionDateTime == null){
        if (get(AcquisitionTime) != null){
                oldAcquisitionDateTime = get(AcquisitionDate) + get(AcquisitionTime)
        } else {
                oldAcquisitionDateTime = get(AcquisitionDate)
        }
        if (oldAcquisitionDateTime.length() > 18){
                oldAcquisitionDateTime = oldAcquisitionDateTime.take(18)
        }
        dateTimeFormat = dateTimeFormat.take(oldAcquisitionDateTime.length())
        newAcquisitionDateTime = new Date().parse(dateTimeFormat,oldAcquisitionDateTime)
}
else {
        dateTimeFormat = dateTimeFormat.take(oldAcquisitionDateTime.length())
    newAcquisitionDateTime = new Date().parse(dateTimeFormat,oldAcquisitionDateTime)
    newAcquisitionDateTime.setDate(newAcquisitionDateTime.getDate() + studyOffset)
}
set(AcquisitionDateTime,newAcquisitionDateTime.format(dateTimeFormat))
log.debug("deid.groovy: New Acquisition Datetime: {}", get(AcquisitionDateTime))

remove(OverlayTime) // (0008,0034)
remove(CurveTime) // (0008,0035)
set(AccessionNumber,newAccessionNumber) // (0008,0050)

remove(IssuerOfAccessionNumberSequence) // blank sequence (0008,0051)
//get(IssuerOfAccessionNumberSequence) // new sequence
//set("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID",newIssuerOfPatientID) // (0040,0031)
//set("IssuerOfAccessionNumberSequence/UniversalEntityID",newUniversalEnityID) // (0040,0032)
//set("IssuerOfAccessionNumberSequence/UniversalEntityIDType",'ISO') // (0040,0033)

remove(InstitutionName) // (0008,0080)
remove(InstitutionAddress) // (0008,0081)
set(ReferringPhysicianName, null) // (0008,0090)
remove(ReferringPhysicianIdentificationSequence) // (0008,0096)
// set(TimezoneOffsetFromUTC, '-0500') // (0008,0201)

// update ProcedureCodeSequence (0008,1032) and use code meanings to create new StudyDescription (0008,1030)
def studyDescription = get(StudyDescription) // (0008,1030)
def today = new Date().format("yyyyMMdd.HHmmss")
def procedureCodes = get(ProcedureCodeSequence)
studyDescription = ''
procedureCodes.each {procedureCode ->
    studyDescription = studyDescription + ' ' + procedureCode.CodeMeaning
}
set(StudyDescription,studyDescription)

// future option: update SeriesDescriptionCodeSequence (0008,103F) and use code meanings to create new SeriesDescription (0008,103F)
// def seriesDescription = get(SeriesDescription) // (0008,103E)
// def newSeriesDescription
// def seriesCodes = get(SeriesDescriptionCodeSequence) 
// seriesCodes.each { seriesCode ->
//      // map procedure code to new value if desired
//      if (newSeriesDescription == null) {
//              newSeriesDescription = seriesCode.get(CodeMeaning)
//      }
//      else {
//              newSeriesDescription = newSeriesDescription + ', ' + seriesCode.get(CodeMeaning)
//      }
// }
// set(SeriesDescription,newSeriesDescription)
remove(NetworkID) // (0008,1000) RETIRED

remove(InstitutionalDepartmentName) // (0008,1040) 
remove(PhysiciansOfRecord) // (0008,1048)
remove(PhysiciansOfRecordIdentificationSequence) // (0008,1049)
remove(PerformingPhysicianName) // (0008,1050)
remove(PerformingPhysicianIdentificationSequence) // (0008,1052)
remove(NameOfPhysiciansReadingStudy) // (0008,1060)
remove(PhysiciansReadingStudyIdentificationSequence) // (0008,1062)
remove(OperatorsName) // (0008,1070)
remove(OperatorIdentificationSequence) // (0008,1072)

def newReferencedStudySequence = get(ReferencedStudySequence) // update UIDs in ReferencedStudySequence (0008,1110)
if (newReferencedStudySequence != []){
    newReferencedStudySequence.each { referencedSOPInstance ->
        // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
        if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
            referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
        }
    }
} else {
    remove(ReferencedStudySequence)
}

def newReferencedPerformedProcedureSequence = get(ReferencedPerformedProcedureStepSequence) // update UIDs in ReferencedPerformedProcedureSequence (0008,1111)
if (newReferencedPerformedProcedureSequence) {
    newReferencedPerformedProcedureSequence.each { referencedProcedure ->
        if (referencedProcedure.get(ReferencedSOPInstanceUID)) {
            referencedProcedure.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedProcedure.get(ReferencedSOPInstanceUID)))
        }
        if (referencedProcedure.get(InstanceCreatorUID)) {
            referencedProcedure.set(InstanceCreatorUID, hasher.hashUID(referencedProcedure.get(InstanceCreatorUID)))
        }        
    }
} else {
    remove(ReferencedPerformedProcedureStepSequence)
}

def newReferencedInstanceSequence = get(ReferencedInstanceSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
newReferencedInstanceSequence.each { referencedSOPInstance ->
    // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
    if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
            referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
    }
}

def newReferencedSeriesSequence = get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
if (newReferencedSeriesSequence != []){
        newReferencedSeriesSequence.each { referencedSeries ->
                // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
                if (referencedSeries.get(SeriesInstanceUID) != null){
                        referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
                }
                newReferencedInstanceSequence = get(ReferencedInstanceSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
                newReferencedInstanceSequence.each { referencedSOPInstance ->
                        // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
                        if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                                referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                        }
                }
        }
} else {
        remove(ReferencedSeriesSequence)
}

def newStudiesContainingOtherReferencedInstancesSequence = get(StudiesContainingOtherReferencedInstancesSequence) // update UIDS in StudiesContainingOtherReferencedInstancesSequence (0008,1200)
if (newStudiesContainingOtherReferencedInstancesSequence != []){
        newStudiesContainingOtherReferencedInstancesSequence.each { referencedStudy ->
                // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
                if (referencedStudy.get(StudyInstanceUID) != null){
                        referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
                }
                newReferencedSeriesSequence = get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
                newReferencedSeriesSequence.each { referencedSeries ->
                        // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
                        if (referencedSeries.get(SeriesInstanceUID) != null){
                                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
                        }
                        newReferencedInstanceSequence = get(ReferencedInstanceSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
                        newReferencedInstanceSequence.each { referencedSOPInstance ->
                                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
                                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                                        referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                                }
                        }
                }
        }
} else {
        remove(StudiesContainingOtherReferencedInstancesSequence)
}

remove(ReferencedPatientSequence) // (0008,1120)

def newReferencedImageSequence = get(ReferencedImageSequence) // update UIDs in ReferencedImageSequence (0008,1140)
if (newReferencedImageSequence != []){
        newReferencedImageSequence.each { referencedSOPInstance ->
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null) {
                        referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                }
        }
} else {
        remove(ReferencedImageSequence)
}

def newRelatedSeriesSequence = get(RelatedSeriesSequence) // update UIDs in RelatedSeriesSequence (0008,1250)
if (newRelatedSeriesSequence != []) {
        newRelatedSeriesSequence.each { referencedSeries ->
                if (referencedSeries.get(StudyInstanceUID) != null){
                        referencedSeries.set(StudyInstanceUID, hasher.hashUID(referencedSeries.get(StudyInstanceUID))) // (0020,000D)
                }
                if (referencedSeries.get(SeriesInstanceUID) != null){
                        referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) // (0020,000E)
                }
        }
} else {
        remove(RelatedSeriesSequence)
}

// ignore DerivationDescription (0008,2111)
// future option: update DerivationCodeSequence (0008,9215) and use code meanings to create new DerivationDescription (0008,2111)
// def newDerivationDescription
// def derivationCodes = get(DerivationCodeSequence) 
// derivationCodes.each { derivationCode ->
//      // map code to new value if desired
//      if (newDerivationDescription == null) {
//              newDerivationDescription = derivationCode.get(CodeMeaning)
//      }
//      else {
//              newDerivationDescription = newDerivationDescription + ', ' + derivationCode.get(CodeMeaning)
//      }
// }
// set(DerivationDescription,newDerivationDescription)

def newSourceImageSequence = get(SourceImageSequence) // update UIDs in ReferencedImageSequence (0008,2112)
if (newSourceImageSequence != []){
        newSourceImageSequence.each { sourceSOPInstance ->
                if (sourceSOPInstance.get(ReferencedSOPInstanceUID) != null) {
                        sourceSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(sourceSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                }
        }
} else {
        remove(SourceImageSequence)
}

// create new UIDs for multivalued IrradiationEventUID (0008,3010)
def newIrradiationEventUIDs = get(IrradiationEventUID)
if (newIrradiationEventUIDs != null){
        if (newIrradiationEventUIDs instanceof List){
                newIrradiationEventUIDs.each { irradiationUID ->
                        log.debug("deid.groovy: Old irradiationUID: {}", irradiationUID)
                        irradiationUID = hasher.hashUID(irradiationUID)
                }
        } else {
                log.debug("deid.groovy: Old irradiationUID: {}", newIrradiationEventUIDs)
                newIrradiationEventUIDs = hasher.hashUID(newIrradiationEventUIDs)
        }
        set(IrradiationEventUID,newIrradiationEventUIDs)
        log.debug("deid.groovy: New irradiationUID: {}", get(IrradiationEventUID))
} else {
        remove(IrradiationEventUID)
}

def newReferencedRawDataSequence = get(ReferencedRawDataSequence) // 0008,9121
if (newReferencedRawDataSequence) {
    newReferencedRawDataSequence.each { referencedStudy ->
        // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
        if (referencedStudy.get(StudyInstanceUID) != null){
                referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
        }
        newReferencedSeriesSequence = referencedStudy.get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
        newReferencedSeriesSequence.each { referencedSeries ->
            // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
            if (referencedSeries.get(SeriesInstanceUID) != null){
                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
            }
            referencedSeries.remove(RetrieveAETitle) // (0008,0054)
            referencedSeries.remove(RetrieveLocationUID) // (0040,E011)
            referencedSeries.remove(RetrieveURL) // (0008,1190)
            referencedSeries.remove(StorageMediaFileSetID) // (0088,0130)
            referencedSeries.remove(StorageMediaFileSetUID) // (0088,0140)
            newReferencedInstanceSequence = get(ReferencedSOPSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
            newReferencedInstanceSequence.each { referencedSOPInstance ->
                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1199)
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                    referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                    referencedSOPInstance.remove(ReferencedDigitalSignatureSequence) // (0040,0402)
                    referencedSOPInstance.remove(ReferencedSOPInstanceMACSequence) // (0040,0403)
                }
            }
        }
    }        
} else {
    remove(ReferencedRawDataSequence)
}

def newReferencedWaveformSequence = get(ReferencedWaveformSequence) // 0008,113A
if (newReferencedWaveformSequence) {
    newReferencedWaveformSequence.each { referencedStudy ->
        // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
        if (referencedStudy.get(StudyInstanceUID) != null){
                referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
        }
        newReferencedSeriesSequence = referencedStudy.get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
        newReferencedSeriesSequence.each { referencedSeries ->
            // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
            if (referencedSeries.get(SeriesInstanceUID) != null){
                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
            }
            referencedSeries.remove(RetrieveAETitle) // (0008,0054)
            referencedSeries.remove(RetrieveLocationUID) // (0040,E011)
            referencedSeries.remove(RetrieveURL) // (0008,1190)
            referencedSeries.remove(StorageMediaFileSetID) // (0088,0130)
            referencedSeries.remove(StorageMediaFileSetUID) // (0088,0140)
            newReferencedInstanceSequence = get(ReferencedSOPSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
            newReferencedInstanceSequence.each { referencedSOPInstance ->
                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1199)
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                    referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                    referencedSOPInstance.remove(ReferencedDigitalSignatureSequence) // (0040,0402)
                    referencedSOPInstance.remove(ReferencedSOPInstanceMACSequence) // (0040,0403)
                }
            }
        }
    }
} else {
    remove(ReferencedWaveformSequence)
}

def newReferencedImageEvidenceSequence = get(ReferencedImageEvidenceSequence) // 0008,9092
if (newReferencedImageEvidenceSequence) {
    newReferencedImageEvidenceSequence.each { referencedStudy ->
        // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
        if (referencedStudy.get(StudyInstanceUID) != null){
                referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
        }
        newReferencedSeriesSequence = referencedStudy.get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
        newReferencedSeriesSequence.each { referencedSeries ->
            // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
            if (referencedSeries.get(SeriesInstanceUID) != null){
                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
            }
            referencedSeries.remove(RetrieveAETitle) // (0008,0054)
            referencedSeries.remove(RetrieveLocationUID) // (0040,E011)
            referencedSeries.remove(RetrieveURL) // (0008,1190)
            referencedSeries.remove(StorageMediaFileSetID) // (0088,0130)
            referencedSeries.remove(StorageMediaFileSetUID) // (0088,0140)
            newReferencedInstanceSequence = get(ReferencedSOPSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
            newReferencedInstanceSequence.each { referencedSOPInstance ->
                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1199)
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                    referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                    referencedSOPInstance.remove(ReferencedDigitalSignatureSequence) // (0040,0402)
                    referencedSOPInstance.remove(ReferencedSOPInstanceMACSequence) // (0040,0403)
                }
            }
        }
    }
} else {
    remove(ReferencedImageEvidenceSequence)
}

def newSourceImageEvidenceSequence = get(SourceImageEvidenceSequence) // 0008,9154
if (newSourceImageEvidenceSequence) {
    newSourceImageEvidenceSequence.each { referencedStudy ->
        // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
        if (referencedStudy.get(StudyInstanceUID) != null){
                referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
        }
        newReferencedSeriesSequence = referencedStudy.get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
        newReferencedSeriesSequence.each { referencedSeries ->
            // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
            if (referencedSeries.get(SeriesInstanceUID) != null){
                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
            }
            referencedSeries.remove(RetrieveAETitle) // (0008,0054)
            referencedSeries.remove(RetrieveLocationUID) // (0040,E011)
            referencedSeries.remove(RetrieveURL) // (0008,1190)
            referencedSeries.remove(StorageMediaFileSetID) // (0088,0130)
            referencedSeries.remove(StorageMediaFileSetUID) // (0088,0140)
            newReferencedInstanceSequence = get(ReferencedSOPSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
            newReferencedInstanceSequence.each { referencedSOPInstance ->
                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1199)
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                    referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                    referencedSOPInstance.remove(ReferencedDigitalSignatureSequence) // (0040,0402)
                    referencedSOPInstance.remove(ReferencedSOPInstanceMACSequence) // (0040,0403)
                }
            }
        }
    }
} else {
    remove(SourceImageEvidenceSequence)
}

def newReferencedPresentationStateSequence = get(ReferencedPresentationStateSequence) // 0008,9237
if (newReferencedPresentationStateSequence) {
    newReferencedPresentationStateSequence.each { referencedStudy ->
        // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
        if (referencedStudy.get(StudyInstanceUID) != null){
                referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
        }
        newReferencedSeriesSequence = referencedStudy.get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
        newReferencedSeriesSequence.each { referencedSeries ->
            // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
            if (referencedSeries.get(SeriesInstanceUID) != null){
                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
            }
            referencedSeries.remove(RetrieveAETitle) // (0008,0054)
            referencedSeries.remove(RetrieveLocationUID) // (0040,E011)
            referencedSeries.remove(RetrieveURL) // (0008,1190)
            referencedSeries.remove(StorageMediaFileSetID) // (0088,0130)
            referencedSeries.remove(StorageMediaFileSetUID) // (0088,0140)
            newReferencedInstanceSequence = get(ReferencedSOPSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
            newReferencedInstanceSequence.each { referencedSOPInstance ->
                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1199)
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                    referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                    referencedSOPInstance.remove(ReferencedDigitalSignatureSequence) // (0040,0402)
                    referencedSOPInstance.remove(ReferencedSOPInstanceMACSequence) // (0040,0403)
                }
            }
        }
    }
} else {
    remove(ReferencedPresentationStateSequence)
}

// ** END OF GROUP 0008 **

// ** GROUP 0010 **
if(clinicalTrialID == null) {
        set(PatientName,"0000^" + newPatientID) // (0010,0010)
} else {
        set(PatientName,clinicalTrialID + "^" + newPatientID) // (0010,0010)
}
set(PatientID,newPatientID) // (0010,0020)
set(IssuerOfPatientID,newIssuerOfPatientID) // (0010,0021)

remove(IssuerOfPatientIDQualifiersSequence) // blank sequence (0010,0024)
get(IssuerOfPatientIDQualifiersSequence) // new empty sequence
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID",newUniversalEnityID) // (0040,0032)
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType",'ISO') // (0040,0033)
log.debug("deid.groovy: New patient domain: {}", get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))

def oldBirthDate = get(PatientBirthDate)
def newBirthDate
if (oldBirthDate != null){
        newBirthDate = oldBirthDate.take(4) + '0101'
        if (newBirthDate.take(4).toInteger() < replacementYear) { // If year is earlier than (current year - 90) set year to (current year - 90)
                newBirthDate = '10660101'
        }
        set(PatientBirthDate,newBirthDate) // (0010,0030)
}

remove(PatientBirthTime) // (0010,0032)
remove(PatientInsurancePlanCodeSequence) // (0010,0050)
remove(PatientPrimaryLanguageCodeSequence) // (0010,0101)
remove(PatientPrimaryLanguageCodeSequence) // (0010,0102)
remove(OtherPatientIDs) // (0010,1000)
remove(OtherPatientNames) // (0010,1001)
remove(OtherPatientIDsSequence) // (0010,1002)
remove(PatientBirthName) // (0010,1005)
remove(PatientAddress) // (0010,1040)
remove(InsurancePlanIdentification) // (0010,1050)
remove(PatientMotherBirthName) // (0010,1060)
remove(MilitaryRank) // (0010,1080)
set(BranchOfService, '')
remove(MedicalRecordLocator) // (0010,1090)
remove(MedicalAlerts) // (0010,2000)
remove(CountryOfResidence) // (0010,2150)
remove(RegionOfResidence) // (0010,2152)
remove(PatientTelephoneNumbers) // (0010,2154)
remove(AdditionalPatientHistory)        // (0010,21B0)
remove(LastMenstrualDate) // (0010,21D0)
remove(PatientReligiousPreference) // (0010,21F0)
remove(ResponsiblePerson) // (0010,2297)
remove(ResponsibleOrganization) // (0010,2299)
remove(PatientComments) // (0010,4000)

// Patient characteristics
if (!patientCharacteristics) {
    remove(PatientSex) // (0010,0040)
    remove(PatientAge) // (0010,1010)
    remove(PatientSize) // (0010,1020)
    remove(PatientWeight) // (0010,1030)    
    remove(Allergies) // (0010,2110)    
    remove(EthnicGroup) // (0010,2160)
    remove(Occupation) // (0010,2180)
    remove(PregnancyStatus) // (0010,21C0)
    remove(PatientSexNeutered) // (0010,2203)
    remove(SmokingStatus) // (0010,21A0)
} else {
    // update PatientAge (0010,1010) if missing or > 90
    def oldPatientAge = get(PatientAge)
    if (oldPatientAge != null){
        if (oldPatientAge.charAt(oldPatientAge.length()-1) == 'Y') {
            def yearAge = oldPatientAge.take(oldPatientAge.length()-1).toInteger()
            if (yearAge > 90) {
                set(PatientAge,'90Y')
            }
        }
    }
}
// ** END OF GROUP 0010 **

// ** GROUP 0012 **
set(ClinicalTrialProtocolID, clinicalTrialID) // (0012,0020)

set(PatientIdentityRemoved, 'YES') // (0012,0062)
def deIDMethod = []
def deIDSequenceCount = 1

get(DeidentificationMethodCodeSequence) // (0012,0064) new empty sequence
set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeValue", '113100') // (0008,0100)
set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeMeaning", 'Basic Application Confidentiality Profile') // (0008,0104)
deIDMethod << 'Basic Application Confidentiality Profile'
deIDSequenceCount +=1

if (dateOffset == 0) {
    set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeValue", '113106') // (0008,0100)
    set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodingSchemeDesignator", 'DCM') // (0008,0102)
    set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeMeaning", 'Retain Longitudinal Temporal Information Full Dates Option') // (0008,0104)
    deIDMethod << 'Retain Longitudinal Temporal Information Full Dates Option'
} else {
    set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeValue", '113107') // (0008,0100)
    set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodingSchemeDesignator", 'DCM') // (0008,0102)
    set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeMeaning", 'Retain Longitudinal Temporal Information Modified Dates Option') // (0008,0104)
    deIDMethod << 'Retain Longitudinal Temporal Information Modified Dates Option'    
}
deIDSequenceCount +=1

if(patientCharacteristics) {
   set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeValue", '113108') // (0008,0100)
   set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodingSchemeDesignator", 'DCM') // (0008,0102)
   set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeMeaning", 'Retain Patient Characteristics Option') // (0008,0104)
    deIDMethod << 'Retain Patient Characteristics Option'
    deIDSequenceCount +=1
}

set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeValue", '113110') // (0008,0100)
set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeMeaning", 'Retain UIDs Option') // (0008,0104)
deIDMethod << 'Retain UIDs Option'
deIDSequenceCount +=1

if(safePrivate) {
  set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeValue", '113111') // (0008,0100)
  set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodingSchemeDesignator", 'DCM') // (0008,0102)
  set("DeidentificationMethodCodeSequence[" + deIDSequenceCount + "]/CodeMeaning", 'Retain Safe Private Option') // (0008,0104)
  deIDMethod << 'Retain Safe Private Option'
  deIDSequenceCount +=1
}

set(DeidentificationMethod, deIDMethod) // (0012,0063)
// ** END GROUP 0012

// ** GROUP 0018 **
// ignore ContrastBolusAgent (0018,0010)
// future option: use ContrastBolusAgentSequence (0018,0012) code meanings to create new ContrastBolusAgent (0008,0010)
// def newContrastBolusAgent
// def contrastAgents = get(ContrastBolusAgentSequence) 
// contrastAgents.each { agent ->
//      // map procedure code to new value if desired
//      if (newContrastBolusAgent == null) {
//              newContrastBolusAgent = agent.get(CodeMeaning)
//      }
//      else {
//              newContrastBolusAgent = newContrastBolusAgent + ', ' + agent.get(CodeMeaning)
//      }
// }
// set(ContrastBolusAgent,newContrastBolusAgent)

// add studyOffset to DateofSecondaryCapture (0018,1012)
set(DateOfSecondaryCapture, dateOffset(get(DateOfSecondaryCapture), studyOffset, newStudyDate))
log.debug("deid.groovy: New DateOfSecondaryCapture: {}", get(DateOfSecondaryCapture))

// hash DeviceSerialNumber (0018,1000) as this is Type 1 for Enhanced SOP Classes
def deviceSerialNumber = get(DeviceSerialNumber)
if (deviceSerialNumber != null) {
    def newDeviceSerialNumber = MessageDigest.getInstance("MD5").digest(deviceSerialNumber.bytes)
    StringBuilder sb3 = new StringBuilder(); // Messy function required by old Groovy version    
    for (byte b : newPatientID) {         
        sb3.append(String.format("%02X", b))
    }     
    set(DeviceSerialNumber, sb3.toString())
}
remove(DeviceUID) // (0018,1002)
remove(PlateID) // (0018,1004)
remove(GeneratorID) // (0018,1005)
remove(CassetteID) // (0018,1007)
remove(GantryID) // (0018,1008)

// future option use PerformedProtocolCodeSequence (0040,0260) code meanings to create new ProtocolName (0018,1030)
// def protocolName = get(ProtocolName) // (0018,1030)
// def newProtocolName
// def protocols = get(PerformedProtocolCodeSequence) 
//protocols.each { protocol ->
//      // map procedure code to new value if desired
//      if (newProtocolName == null) {
//              newProtocolName = protocol.get(CodeMeaning)
//      }
//      else {
//              newProtocolName = newProtocolName + ', ' + protocol.get(CodeMeaning)
//      }
//      def protocolContext = get(ProtocolContextSequence) // (0040,0440)
//      protocolContext.each { context ->
//              newProtocolName = newProtocolName + ' ' + context.get(CodeMeaning)
//              def contextModifiers = get(ContentItemModifierSequence) // (0040,0441)
//              contextModifiers.each {modifier ->
//                      newProtocolName = newProtocolName + '(' + modifier.get(CodeMeaning) + ')'
//              }
//      }
// }
// set(ProtocolName,newProtocolName)

// ignore AcquisitionDeviceProcessingDescription (0018,1400)

remove(DetectorID) // (0018,700A)

// ignore AcquisitionProtocolDescription (0018,9424)

remove(ContributingEquipmentSequence) // (0018,A001)
// ** END OF GROUP 0018 **

// ** GROUP 0020 **
def oldStudyInstanceUID = get(StudyInstanceUID)
if (oldStudyInstanceUID != null){
    set(StudyInstanceUID, hasher.hashUID(oldStudyInstanceUID) + flagUIDSuffix ) // (0020,000D)
}

def oldSeriesInstanceUID = get(SeriesInstanceUID)
if (oldSeriesInstanceUID != null) {
        set(SeriesInstanceUID, hasher.hashUID(oldSeriesInstanceUID)) // (0020,000E)
}

set(StudyID, newAccessionNumber) // (0020,0010)

def oldFrameOfReferenceUID = get(FrameOfReferenceUID)
if (oldFrameOfReferenceUID != null){
        set(FrameOfReferenceUID, hasher.hashUID(oldFrameOfReferenceUID)) // (0020,0052) 
}

def oldSynchronizationFrameOfReferenceUID = get(SynchronizationFrameOfReferenceUID)
if (oldSynchronizationFrameOfReferenceUID != null){
        set(SynchronizationFrameOfReferenceUID, hasher.hashUID(oldSynchronizationFrameOfReferenceUID)) // (0020,0200)
} else {
        remove(SynchronizationFrameOfReferenceUID)
}

remove(ImageComments) // (0020,4000)

def oldConcatenationUID = get(ConcatenationUID) // Update ConcatentationUID (0020,9161)
if (oldConcatenationUID != null){
        set(ConcatenationUID, hasher.hashUID(oldConcatenationUID)) // (0020,9161)
} else {
        remove(ConcatenationUID)
}

def oldConcatenationSource = get(SOPInstanceUIDOfConcatenationSource) // (0020,0242)
if (oldConcatenationSource != null) {
    set(SOPInstanceUIDOfConcatenationSource, hasher.hashUID(oldConcatenationSource))
} else {
    remove(SOPInstanceUIDOfConcatenationSource)
}

def newDimensionOrganizationSequence = get(DimensionOrganizationSequence) // update UIDs in DimensionOrganizationSequence (0020,9221)
if (newDimensionOrganizationSequence != []) {
        newDimensionOrganizationSequence.each { dimensionOrganization ->
                // create new UID for DimensionOrganization.DimensionOrganizationUID (0020,9164)        
                if (dimensionOrganization.get(DimensionOrganizationUID) != null){
                        dimensionOrganization.set(DimensionOrganizationUID, hasher.hashUID(dimensionOrganization.get(DimensionOrganizationUID)))
                }
        }
} else {
        remove(DimensionOrganizationSequence)
}

def newDimensionIndexSequence = get(DimensionIndexSequence) // update UIDs in DimensionIndexSequence (0020,9222)
if (newDimensionIndexSequence != []){
        newDimensionIndexSequence.each { dimensionIndex ->
                // create new UID for dimensionIndex.DimensionOrganizationUID (0020,9164)
                if (dimensionIndex.get(DimensionOrganizationUID) != null) {
                        dimensionIndex.set(DimensionOrganizationUID, hasher.hashUID(dimensionIndex.get(DimensionOrganizationUID)))      
                }
        }
} else {
        remove(DimensionIndexSequence)
}
// ** END OF GROUP 0020 **

set(LongitudinalTemporalInformationModified, 'MODIFIED') // (0028,0303)
//      ** END OF GROUP 0028 **

// ** GROUP 0032 **
remove(RequestedProcedureDescription) // (0032,1060)
remove(RequestingPhysician) // (0032,1032)
remove(RequestingService) // 0032,1033
remove(StudyPriorityID) // (0032,000C) RETIRED
remove(StudyComments) // 0032,4000 RETIRED
// ** END OF GROUP 0032

// ** GROUP 0038 **
remove(AdmissionID) // (0038,0010)
remove(CurrentPatientLocation) // (0038,0300)
remove(IssuerOfAdmissionID) // (0038,0011)
remove(IssuerOfAdmissionIDSequence) // (0038,0014)
remove(ServiceEpisodeID) // (0038,0060)
remove(IssuerOfServiceEpisodeID) // (0038,0061)
remove(ServiceEpisodeDescription) // (0038,0062)
remove(IssuerOfServiceEpisodeIDSequence) // (0038,0064)
// Patient characteristics
if (!patientCharacteristics) {
    remove(SpecialNeeds) // (0038,0050)
    remove(PatientState) // (0038,0500)
}
// ** END OF GROUP 0038 **

// ** GROUP 0040 **
// add studyOffset to ScheduledProcedureStepStartDate (0040,0002)
set(ScheduledProcedureStepStartDate, dateOffset(get(ScheduledProcedureStepStartDate), studyOffset, newStudyDate))

// add studyOffset to ScheduledProcedureStepEndDate (0040,0003)
set(ScheduledProcedureStepEndDate, dateOffset(get(ScheduledProcedureStepEndDate),studyOffset, newStudyDate))

// add studyOffset to PerformedProcedureStepStartDate (0040,0244)
set(PerformedProcedureStepStartDate, dateOffset(get(PerformedProcedureStepStartDate), studyOffset, newStudyDate))

// add studyOffset to PerformedProcedureStepEndDate (0040,0250)
set(PerformedProcedureStepEndDate, dateOffset(get(PerformedProcedureStepEndDate),studyOffset,newStudyDate))

remove(PerformedProcedureStepID) // (0040,0253)
remove(PerformedProcedureStepDescription) // (0040,0254)

def newRequestAttributeSequence = get(RequestAttributesSequence) // update RequestAttributesSequence (0040,0275)
if (newRequestAttributeSequence != []) {
    newRequestAttributeSequence.each { request ->
        request.remove(AccessionNumber) // (0008,0050)
        request.remove(IssuerOfAccessionNumberSequence) // (0008,0051)
        request.remove(ScheduledProcedureStepID) //(0040,0009)
        request.remove(RequestedProcedureID) // (0040,1001)
        def oldRequestStudyInstanceUID = request.get(StudyInstanceUID)
        if (oldRequestStudyInstanceUID != null) {
            request.set(StudyInstanceUID, hasher.hashUID(oldStudyInstanceUID)) // (0020,000D)
        }
        newReferencedStudySequence = request.get(ReferencedStudySequence) // update UIDs in ReferencedStudySequence (0008,1110)
        newReferencedStudySequence.each { referencedSOPInstance ->
            // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
            referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
        }
    }
    set(RequestAttributesSequence,newRequestAttributeSequence)
} else {
    remove(RequestAttributesSequence)
}

remove(CommentsOnThePerformedProcedureStep) // (0040,0280)
remove(RequestedProcedureID) // (0040,1001)

// Consider morphing SpecimenUID (0040,0554), though it is not specified in PS3.15

remove(AcquisitionContextSequence) // removes existing sequence (0040,0555)
get(AcquisitionContextSequence) // creates blank sequence

// Consider transforming ReasonForRequestedProcedureCodeSequence (0040,1012) values

remove(FillerOrderNumberImagingServiceRequest) // (0040,2017)

// Patient Characteristics
if (!patientCharacteristics) {
    remove(PreMedication) // (0040,0012)
}
if(clinicalTrialID != null) {
    set(ConfidentialityCode, clinicalTrialID)
} else {
    set(ConfidentialityCode, '')
}
// ** END OF GROUP 0040

// ** GROUP 0050 **
def newDeviceSequence = get(DeviceSequence) // remove serial number from DeviceSequence (0050,0010)
if (newDeviceSequence != []) {
        newDeviceSequence.each { device ->
                device.remove(DeviceSerialNumber) // (0018,1000)
                device.remove(DeviceID) // (0018,1003)
        }
        set(DeviceSequence, newDeviceSequence)
} else {
        remove(DeviceSequence)
}
// ** END OF GROUP 0050

// ** GROUP 0088 **
remove(IconImageSequence) // (0088,0200)
// ** END OF GROUP 0088 **

// ** GROUP 0400 **
remove(OriginalAttributesSequence) // (0400,0561)
// ** END OF GROUP 0400 **

// ** GROUP 5200 **
def newSharedFunctionalGroups = get(SharedFunctionalGroupsSequence)
if (newSharedFunctionalGroups) {
    newReferencedImageSequence = get('SharedFunctionalGroupsSequence/ReferencedImageSequence') // update UIDs in ReferencedImageSequence (0008,1140)
    if (newReferencedImageSequence) {
        newReferencedImageSequence.each { referencedSOPInstance ->
            if (referencedSOPInstance.get(ReferencedSOPInstanceUID)) {
                referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
            }
        }
    } 
    def newDerivationImageSequence = get('SharedFunctionalGroupsSequence/DerivationImageSequence')
    if (newDerivationImageSequence) {
        newDerivationImageSequence.each { derivationImage ->
            newSourceImageSequence = derivationImage.get(SourceImageSequence) // update UIDs in ReferencedImageSequence (0008,2112)
            if (newSourceImageSequence){
                newSourceImageSequence.each { sourceSOPInstance ->
                    if (sourceSOPInstance.get(ReferencedSOPInstanceUID) != null) {
                        sourceSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(sourceSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                    }
                }
            } else {
                    derivationImage.remove(SourceImageSequence)
            }
        }
    }
} else {
    remove(SharedFunctionalGroupsSequence)
}

def newPerFrameFunctionalGroups = get(PerFrameFunctionalGroupsSequence)
if (newPerFrameFunctionalGroups) {
    newPerFrameFunctionalGroups.each { functionalGroup ->
        newReferencedImageSequence = functionalGroup.get(ReferencedImageSequence) // update UIDs in ReferencedImageSequence (0008,1140)
        if (newReferencedImageSequence) {
            newReferencedImageSequence.each { referencedSOPInstance ->
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID)) {
                    referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                }
            }
        } else {
            functionalGroup.remove(ReferencedImageSequence)
        }
        newDerivationImageSequence = functionalGroup.get(DerivationImageSequence)
        if (newDerivationImageSequence) {
            newDerivationImageSequence.each { derivationImage ->
                newSourceImageSequence = derivationImage.get(SourceImageSequence) // update UIDs in ReferencedImageSequence (0008,2112)
                if (newSourceImageSequence){
                    newSourceImageSequence.each { sourceSOPInstance ->
                        if (sourceSOPInstance.get(ReferencedSOPInstanceUID) != null) {
                            sourceSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(sourceSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                        }
                    }
                } else {
                        derivationImage.remove(SourceImageSequence)
                }
            }
        } else {
            functionalGroup.remove(DerivationImageSequence)
        }
    }
} else {
    remove(PerFrameFunctionalGroupsSequence)
}
// ** END OF GROUP 5200 **

// ** GROUP 60XX **
if (!retainOverlays) {
    remove(OverlayRows) // (60xx,0010)
    remove(OverlayColumns) // (60xx,0011)
    remove(OverlayDescription) // (60xx,0022)
    remove(OverlayType) // (60xx,0040)
    remove(OverlaySubtype) // (60xx,0045)
    remove(OverlayOrigin) // (60xx,0050)
    remove(OverlayBitsAllocated) // (60xx,0100)
    remove(OverlayBitPosition) // (60xx,0102)
    remove(OverlayLabel) // (60xx,1500)
    remove(OverlayData) // (60XX,3000)
}
// ** END OF GROUP 60XX **

// ** GROUP FFFA **
remove(DigitalSignaturesSequence) // (FFFA,FFFA)
// ** END OF GROUP FFFA

// *******************************
// ** END OF ATTRIBUTE MORPHING **
// *******************************

// ** PRIVATE TAG REMOVAL **

def privateGroups = [
//  [PrivateCreator,
//      [
//          0xggggeeee,
//          0xggggeeee,
//          ...
//      ]
//  ] 
//  ** SIEMENS **
//  (0019,00xx) SIEMENS MR HEADER
    ['SIEMENS MR HEADER',
        [
            0x00191008,
            0x00191009,
            0x0019100b,
            0x0019100f,
            0x00191010,
            0x00191011,
            0x00191012,
            0x00191013,
            0x00191014,
            0x00191015,
            0x00191016,
            0x00191017,
            0x00191018
        ]
    ],
//  (0029,00xx) SIEMENS CSA HEADER
    ['SIEMENS CSA HEADER',
        [
            0x00291008,
            0x00291009,
            0x00291010,                        
            0x00291018,
            0x00291019,
            0x00291020
        ]
    ],
//  (0029,00xx) SIEMENS CSA NON-IMAGE
    ['SIEMENS CSA NON-IMAGE',
        [
            0x00291008,
            0x00291009,
            0x00291010
        ]
    ],
//  (0029,00xx) SIEMENS MEDCOM HEADER
    ['SIEMENS MEDCOM HEADER',
        [
            0x00291008,
            0x00291009,
            0x00291010,
            0x00291020,
            0x00291040,
            0x00291041,                                    
            0x00291042,
            0x00291043,
            0x00291044,                 
            0x00291050,
            0x00291051,
            0x00291052,
            0x00291053,
            0x00291054,
            0x00291055,
            0x00291070,
            0x00291071,
            0x00291072,
            0x00291073,
            0x00291074,
            0x00291075,
            0x00291076  
        ]                  
    ],    
//  (0029,00xx) SIEMENS MEDCOM HEADER2
    ['SIEMENS MEDCOM HEADER2',
        [
            0x00291060
        ]
    ],
//  (0029,00xx) SIEMENS MEDCOM OOG
    ['SIEMENS MEDCOM OOG',
        [
            0x00291008,
            0x00291009,
            0x00291010
        ]
    ],
//  (0051,00xx) SIEMENS MR HEADER
    ['SIEMENS MR HEADER',
        [
            0x00511008,
            0x00511009,
            0x0051100a,
            0x0051100b,
            0x0051100c,
            0x0051100d,
            0x0051100e,
            0x0051100f,
            0x00511011,
            0x00511012,
            0x00511013,
            0x00511015,
            0x00511016,
            0x00511017,
            0x00511019
        ]
    ],
//  (7fe1,00xx) SIEMENS CSA NON-IMAGE
    ['SIEMENS CSA NON-IMAGE',
        [
            0x7fe10010
        ]
    ]    
]

def rawDicom = input.getDicomObject()
def privateTagWhitelist = []
if (!safePrivate) {
    // No whitelist allowed if profile 113111 is not included
    privateGroups = []
}
if (rawDicom != null){
    // GENERATE PRIVATE ELEMENT WHITELIST
    privateGroups.each {group ->
        privateCreator = group[0]
        privateTags = group[1]
        privateTags.each {privateAttribute ->
            privateTag = rawDicom.resolveTag(privateAttribute,privateCreator)
            if (rawDicom.get(privateTag) != null){
                log.debug("deid.groovy: Whitelisting {}:0x{}", privateCreator, Integer.toHexString(privateAttribute))
                privateTagWhitelist << Integer.toHexString(privateTag)
            }
        }
    }
    log.debug("deid.groovy: Whitelist:{}", privateTagWhitelist)
    // REMOVE NON-WHITELIST PRIVATE ELEMENTS
    def privateGroupNumber = 0x0005
    while (privateGroupNumber <= 0xFFFF) {
        def groupStart = (privateGroupNumber * 0x10000) + 0x0001
        def groupEnd = (privateGroupNumber * 0x10000) + 0xFFFF
        privateGroup = rawDicom.iterator(groupStart, groupEnd)
        privateGroup.each{ privateElement ->
            log.debug("deid.groovy: Private element found: {}", privateElement)
            if ( (privateElement.tag() % 0x10000) > 0x1000) {   
                if (!privateTagWhitelist.contains(privateElement.tag())){
                    privateTag = privateElement.tag()
                    privateCreator = (privateTag.intdiv(0x10000) * 0x10000) + (privateTag % 0x10000).intdiv(0x100)
                    // newPrivateAttributes << get(privateCreator) + ':' + Integer.toHexString(privateTag)
                    remove(privateElement.tag())
                }
            }
        }
        privateGroupNumber += 0x0002
    }
    // Remove private attributes from shared functional groups macro
    if(newSharedFunctionalGroups) {
        rawDicom = newSharedFunctionalGroups[0].getDicomObject()
        privateGroupNumber = 0x0005
        while (privateGroupNumber <= 0xFFFF) {
            groupStart = (privateGroupNumber * 0x10000) + 0x0001
            groupEnd = (privateGroupNumber * 0x10000) + 0xFFFF
            privateGroup = rawDicom.iterator(groupStart, groupEnd)
            privateGroup.each{ privateElement ->
                log.debug("deid.groovy: Private element found: {}", privateElement)
                if ( (privateElement.tag() % 0x10000) > 0x1000) {   
                    if (!privateTagWhitelist.contains(privateElement.tag())){
                        privateTag = privateElement.tag()
                        privateCreator = (privateTag.intdiv(0x10000) * 0x10000) + (privateTag % 0x10000).intdiv(0x100)
                        // newPrivateAttributes << get(privateCreator) + ':' + Integer.toHexString(privateTag)
                        newSharedFunctionalGroups[0].remove(privateElement.tag())
                    }
                }
            }
            privateGroupNumber += 0x0002
        }
    }
    // Remove private attributes from shared functional groups macro
    if(newPerFrameFunctionalGroups) {
        newPerFrameFunctionalGroups.each { privateFunctionalGroup ->
            rawDicom = privateFunctionalGroup.getDicomObject()
            privateGroupNumber = 0x0005
            while (privateGroupNumber <= 0xFFFF) {
                groupStart = (privateGroupNumber * 0x10000) + 0x0001
                groupEnd = (privateGroupNumber * 0x10000) + 0xFFFF
                privateGroup = rawDicom.iterator(groupStart, groupEnd)
                privateGroup.each{ privateElement ->
                    log.debug("deid.groovy: Private element found: {}", privateElement)
                    if ( (privateElement.tag() % 0x10000) > 0x1000) {   
                        if (!privateTagWhitelist.contains(privateElement.tag())){
                            privateTag = privateElement.tag()
                            privateCreator = (privateTag.intdiv(0x10000) * 0x10000) + (privateTag % 0x10000).intdiv(0x100)
                            // newPrivateAttributes << get(privateCreator) + ':' + Integer.toHexString(privateTag)
                            privateFunctionalGroup.remove(privateElement.tag())
                        }
                    }
                }
                privateGroupNumber += 0x0002
            }   
        }
    }     
}

// ** END OF PRIVATE TAG REMOVAL **

log.info("deid.groovy: Completed de-identification")
